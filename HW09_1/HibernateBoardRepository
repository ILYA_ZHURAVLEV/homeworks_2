package org.levelup.trello.service.hibernate;


import org.hibernate.SessionFactory;
import org.levelup.trello.model.Board;
import org.levelup.trello.model.BoardColumn;
import org.levelup.trello.model.User;
import org.levelup.trello.service.jdbc.BoardRepository;

import java.util.Collection;


public class HibernateBoardRepository extends AbstractHibernateRepository implements BoardRepository {

    public  HibernateBoardRepository(SessionFactory factory) {
        super(factory);
    }


    public Board createBoard(Integer userId, String name, boolean favourite) {
        return runWithTransaction(session -> {
            Board board = new Board();
            board.setName(name);
            board.setFavourite(favourite);

            User user = session.load(User.class, userId);
            board.setOwner(user);

            session.persist(board);
            return board;
        });
    }

    public Board addColumn(Integer boardId, String name, int columnOrder) {
        return runWithTransaction(session -> {
            BoardColumn column = new BoardColumn();
            column.setName(name);
            column.setOrder(columnOrder);
            Board board = session.get(Board.class, boardId);
            board.getColumns().add(column);
            return board;
        });
    }

    public Board updateBoard(Integer boardId, String name, Boolean favourite) {
        return runWithTransaction(session -> {
            Board board = session.get(Board.class, boardId);
            board.setName(name);
            board.setFavourite(favourite);
            return board;
        });
    }

    public Board deleteBoard(Integer boardId) {
        return runWithTransaction(session -> {

            Board board = session.get(Board.class, boardId);

            session.createNativeQuery("delete from columns as c where c.board_id = ?")
                    .setParameter(1, boardId)
                    .executeUpdate();

            session.createQuery("delete from Board where id =: boardId")
                    .setParameter("boardId", boardId)
                    .executeUpdate();

            return board;
        });
    }

}
