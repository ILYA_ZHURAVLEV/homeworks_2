package org.levelup.trello.service.hibernate;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.levelup.trello.model.BoardColumn;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

public class HibernateBoardColumnRepositoryTest {

    private SessionFactory factory;
    private Session session;
    private Transaction transaction;

    private HibernateBoardColumnRepository hibernateBoardColumnRepository;

    @BeforeEach
    public void setupRepository() {
        factory = Mockito.mock(SessionFactory.class);
        session = Mockito.mock(Session.class);
        Mockito.when(factory.openSession()).thenReturn(session);

        transaction = Mockito.mock(Transaction.class);
        Mockito.when(session.beginTransaction()).thenReturn(transaction);

        hibernateBoardColumnRepository = new HibernateBoardColumnRepository(factory);
    }

    @Test
    public void testUpdateBoardColumn_whenAllDataIsValid_thenReturnBoardColumn() {
        // given
        Integer id = 3;
        String name = "New Name";
        Integer order = 5;

        BoardColumn boardColumn = new BoardColumn();
        boardColumn.setId(id);
        Mockito.when(session.get(BoardColumn.class, id))
                .thenReturn(boardColumn);

        // when
        BoardColumn result = hibernateBoardColumnRepository.updateBoardColumn(id, name, order);

        //then
        Assertions.assertEquals(result.getId(), id);
        Assertions.assertEquals(result.getName(), name);
        Assertions.assertEquals(result.getOrder(), order);
    }

    @Test
    public void testDeleteBoardColumn_whenAllDataIsValid_thenReturnBoardColumn() {
        // given
        Integer id = 4;
        BoardColumn boardColumn = new BoardColumn();
        boardColumn.setId(id);
        Mockito.when(session.get(BoardColumn.class, id))
                .thenReturn(boardColumn);
        Query query = Mockito.mock(Query.class);
        Mockito.when(session.createQuery("delete from BoardColumn where id =: columnId"))
                .thenReturn(query);
        Mockito.when(query.setParameter("columnId", id))
                .thenReturn(query);

        // when
        BoardColumn result = hibernateBoardColumnRepository.deleteBoardColumn(id);

        //then
        Mockito.verify(query).executeUpdate();
        Assertions.assertEquals(result.getId(), id);
    }

}